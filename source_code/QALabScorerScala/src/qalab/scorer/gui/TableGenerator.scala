package qalab.scorer.gui

import qalab.scorer.ExamEnum

/**
 * @author K.Sakamoto
 */
object TableGenerator {
  def getTHs(exam : ExamEnum.Value, isActivated : Boolean) = {
    val idColumn = exam match {
      case ExamEnum.CENTER_EXAM =>
        "anscolumn_ID"
      case ExamEnum.SECONDARY_EXAM =>
        "answer_section's id attribute</th><th>answer_section's label attribute"
    }
    if (isActivated) {
      val scoreColumn = exam match {
        case ExamEnum.CENTER_EXAM =>
          "<th>SCORE</th>"
        case ExamEnum.SECONDARY_EXAM =>
          ""
      }
      s"<tr><th>$idColumn</th><th>RESULT</th><th>YOUR ANSWER</th><th>CORRECT ANSWER</th>$scoreColumn</tr>"
    } else {
      s"<tr><th>$idColumn</th><th>YOUR ANSWER</th></tr>"
    }
  }
}
