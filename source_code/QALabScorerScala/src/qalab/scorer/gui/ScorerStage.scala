package qalab.scorer.gui

import java.awt.Desktop
import java.io._
import java.nio.charset.StandardCharsets
import java.nio.file.{Files, Paths}
import javafx.application.Application
import javafx.event.{ActionEvent, EventHandler}
import javafx.scene.control.{Button, Label}
import javafx.scene.image.{Image, ImageView}
import javafx.stage.FileChooser.ExtensionFilter
import javafx.stage.{FileChooser, Stage}

import org.apache.velocity.VelocityContext
import org.apache.velocity.app.Velocity
import qalab.scorer._

import scala.xml.{PrettyPrinter, NodeSeq}

/**
 * @author K.Sakamoto
 */
class ScorerStage extends Application {
  private val title = "NTCIR QALab Simple Scorer"

  private def getTitle(subTitle : String) = s"$title - $subTitle"

  private def convertXMLtoString(xml : NodeSeq) =
    xml match {
      case NodeSeq.Empty =>
        "<pre></pre>"
      case otherwise =>
        val printer = new PrettyPrinter(40, 2)
        val builder = new StringBuilder
        printer.format (otherwise.head, builder)
        "<pre>%s</pre>".format (builder.result().
          replaceAll ("<", "&lt;").
          replaceAll (">", "&gt;"))
    }

  override def start(primaryStage : Stage) {
    val submitButton = new Button
    submitButton setText "Submit"
    submitButton setOnAction new EventHandler[ActionEvent] {
      override def handle(event : ActionEvent) {
        val subStage = new Stage
        val fileChooser = new FileChooser
        fileChooser setTitle getTitle("Answer Sheet Submitter")
        fileChooser setInitialDirectory
          new File(System getProperty "user.home")
        fileChooser.getExtensionFilters add
          new ExtensionFilter("XML", "*.xml")
        Option(fileChooser showOpenDialog subStage) foreach {
          case file if file.canRead =>
            val builder    = new StringBuilder
            val filePath   = file.toPath.toAbsolutePath.toString
            val outputFile = new File(ExamDataEnum.removeDotXML(filePath) concat ".result.html")
            MyLogFile clearLogFile()
            val correctAnswerFile = ExamDataEnum getCorrectAnswersFile file
            val fileName     = correctAnswerFile.toPath.getFileName.toString
            val examDataEnum = ExamDataEnum getExamData ExamDataEnum.removeDotXML(fileName)
            val examEnum     = ExamDataEnum getExam     examDataEnum
            Validator validate (filePath, examEnum)
            val writer = new PrintWriter(Files newBufferedWriter (outputFile.toPath, StandardCharsets.UTF_8))
            examDataEnum match {
              case ExamDataEnum.NONE =>
                val pFormat = "<p>%s</p>"
                builder.append("<p>ERROR: Check the file name.</p>").
                        append(pFormat format file.toPath.getFileName.toString).
                        append(pFormat format ExamDataEnum.removeDotXML(fileName)).
                        append("<ul>")
                val liFormat = "<li>%s</li>"
                ExamDataEnum.getAllExamData foreach {
                  examData => builder append (liFormat format
                    (ExamDataEnum getCorrectAnswersFileName examData))
                }
                ExamDataEnum.getAdditionalFiles foreach {
                  f => builder append (liFormat format f)
                }
                builder append "</ul>"
              case examData =>
                Velocity setProperty(
                  "file.resource.loader.path",
                  (Paths get ("res", "vm")).toAbsolutePath.toString)
                Velocity init()

                val exam = ExamDataEnum getExam examData
                val log = MyLogFile.getLog
                val isValid = log == ""
                if (isValid && (ExamDataEnum isActivated file)) {
                  exam match {
                    case ExamEnum.OTHER =>
                      builder append "ERROR: "
                    case ExamEnum.SECONDARY_EXAM =>
                      val correctionTable = new StringBuilder
                      val corrections = Scorer.getCorrections(file, exam, examData)
                      val keySeq = corrections.keySet.toSeq.sorted
                      var correctAnswerCount     = 0
                      var incorrectAnswerCount   = 0
                      var withholdingAnswerCount = 0
                      var unansweredAnswerCount  = 0
                      var errorAnswerCount       = 0
                      correctionTable.append(TableGenerator.getTHs(exam, isActivated = true))
                      val format = "<tr class=\"%s\">%s<td>%s</td><td>%s</td><td>%s</td></tr>%n"
                      for (i <- keySeq.indices) {
                        val key = keySeq(i)
                        val (mark, systemAnswer, correctAnswer) = corrections(key)
                        val trClass = mark match {
                          case MarkEnum.CORRECT     => "correct"
                          case MarkEnum.INCORRECT   => "incorrect"
                          case MarkEnum.UNANSWERED  => "unanswered"
                          case MarkEnum.WITHHOLDING => "withholding"
                          case MarkEnum.ERROR       => "error"
                        }
                        correctionTable append (
                          format format (
                            trClass,
                            correctAnswer._1,
                            MarkEnum toString mark,
                            convertXMLtoString (systemAnswer._2),
                            convertXMLtoString (correctAnswer._2)
                            ))
                        mark match {
                          case MarkEnum.CORRECT =>
                            correctAnswerCount += 1
                          case MarkEnum.INCORRECT =>
                            incorrectAnswerCount += 1
                          case MarkEnum.WITHHOLDING =>
                            withholdingAnswerCount += 1
                          case MarkEnum.UNANSWERED =>
                            unansweredAnswerCount += 1
                          case _ =>
                            errorAnswerCount += 1
                        }
                      }
                      val context = new VelocityContext
                      context put ("title",       title)
                      context put ("correct",     correctAnswerCount)
                      context put ("incorrect",   incorrectAnswerCount)
                      context put ("withholding", withholdingAnswerCount)
                      context put ("unanswered",  unansweredAnswerCount)
                      context put ("error",       errorAnswerCount)
                      context put ("corrections", correctionTable result())
                      val template = Velocity getTemplate (
                        "corrections_for_second_stage_exam.vm",
                        StandardCharsets.UTF_8.name())
                      val stringWriter = new StringWriter
                      template merge (context, stringWriter)
                      stringWriter flush()
                      builder append (stringWriter toString)
                    case ExamEnum.CENTER_EXAM =>
                      val correctionTable = new StringBuilder
                      val corrections = Scorer getCorrections (file, exam, examData)
                      val keySeq = corrections.keySet.toSeq.sorted
                      var yourScore              = 0D
                      var totalScore             = 0D
                      var correctAnswerCount     = 0
                      var incorrectAnswerCount   = 0
                      var withholdingAnswerCount = 0
                      var unansweredAnswerCount  = 0
                      var errorAnswerCount       = 0
                      correctionTable append (TableGenerator getTHs (exam, true))
                      val format = "<tr class=\"%s\">%s<td>%s</td><td>%s</td><td>%s</td><td>%.2f</td></tr>%n"
                      for (i <- keySeq.indices) {
                        val key = keySeq(i)
                        val (mark, systemAnswer, correctAnswer) = corrections(key)
                        var score = correctAnswer._3
                        val trClass = mark match {
                          case MarkEnum.CORRECT     => "correct"
                          case MarkEnum.INCORRECT   => "incorrect"
                          case MarkEnum.UNANSWERED  => "unanswered"
                          case MarkEnum.WITHHOLDING => "withholding"
                          case MarkEnum.ERROR       => "error"
                        }
                        correctionTable append (
                          format format (
                            trClass,
                            correctAnswer._1,
                            MarkEnum toString mark,
                            convertXMLtoString (systemAnswer._2),
                            convertXMLtoString (correctAnswer._2),
                            score))
                        totalScore += score
                        mark match {
                          case MarkEnum.CORRECT =>
                            correctAnswerCount += 1
                            yourScore += score
                          case MarkEnum.INCORRECT =>
                            incorrectAnswerCount += 1
                          case MarkEnum.WITHHOLDING =>
                            withholdingAnswerCount += 1
                          case MarkEnum.UNANSWERED =>
                            unansweredAnswerCount += 1
                          case _ =>
                            errorAnswerCount += 1
                        }
                      }
                      val context = new VelocityContext
                      context put ("title",       title)
                      context put ("yourScore",   yourScore)
                      context put ("totalScore",  totalScore)
                      context put ("correct",     correctAnswerCount)
                      context put ("incorrect",   incorrectAnswerCount)
                      context put ("withholding", withholdingAnswerCount)
                      context put ("unanswered",  unansweredAnswerCount)
                      context put ("error",       errorAnswerCount)
                      context put ("corrections", correctionTable result())
                      val template = Velocity getTemplate (
                        "corrections_for_national_center_test.vm",
                        StandardCharsets.UTF_8.name())
                      val stringWriter = new StringWriter
                      template merge (context, stringWriter)
                      stringWriter flush()
                      builder append (stringWriter toString)
                  }
                } else {
                  val validationTable = new StringBuilder
                  validationTable append (TableGenerator getTHs (exam, false))
                  val context = new VelocityContext
                  context put("title", title)

                  if (isValid) {
                    val format = "<tr>%s<td>%s</td></tr>%n"
                    println(file)
                    println(exam)
                    val validations = Scorer.getValidations(file, exam)
                    val keySeq = validations.keySet.toSeq.sorted
                    val size = keySeq.size
                    for (i <- 0 until size) {
                      val key = keySeq(i)
                      val validation = validations(key)
                      validationTable append (format format(
                        validation._1 toString(),
                        convertXMLtoString(validation._2)))
                    }
                    context put("the_number_of_answers", size)
                    context put("validations",           validationTable result())
                  } else {
                    context put("the_number_of_answers", -1)
                    context put("validations",
                      "<tr class=\"error\"><td>%s</td></tr>" format log)
                  }

                  val template = Velocity getTemplate ("validations.vm", StandardCharsets.UTF_8.name())
                  val stringWriter = new StringWriter
                  template merge (context, stringWriter)
                  stringWriter flush()
                  builder append (stringWriter toString)
                }
            }
            try {
              val s = builder result()
              //println(s)
              writer.println(s)
            } catch {
              case e : IOException =>
                e printStackTrace()
            } finally {
              try {
                writer close()
              } catch {
                case e : IOException =>
                  e printStackTrace()
              }
            }
            if (Desktop.isDesktopSupported) {
              (Desktop getDesktop) browse (outputFile toURI)
            }
        }
      }
    }

    val answerSheetLabel = new Label("Answer Sheet")
    val image = new Image(new FileInputStream(
      Paths.get("res", "image", "qalab.png").toFile))
    val qaLabIcon = new Label("", new ImageView(image))

    primaryStage setTitle title
    Controller replaceSceneContent (primaryStage,
      new HomeController(
        primaryStage,
        qaLabIcon,
        answerSheetLabel,
        submitButton))
    primaryStage show()
  }
}