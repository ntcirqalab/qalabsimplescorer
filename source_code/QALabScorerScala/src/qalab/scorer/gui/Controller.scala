package qalab.scorer.gui

import java.awt.Desktop
import java.net.URI
import javafx.event.EventHandler
import javafx.geometry.{Insets, Pos}
import javafx.scene.{Scene, Parent}
import javafx.scene.control.{Hyperlink, Button, Label}
import javafx.scene.input.MouseEvent
import javafx.scene.layout.GridPane
import javafx.stage.Stage

/**
 * @author K.Sakamoto
 */
object Controller {
  def replaceSceneContent(stage : Stage, controller : Parent) {
    var scene = stage.getScene
    Option(scene) match {
      case Some(s) =>
        stage.getScene.setRoot(controller)
      case None =>
        scene = new Scene(controller, 300, 300)
        stage setScene scene
    }
  }
}

class HomeController(stage : Stage, qaLabIcon : Label, answerSheetLabel : Label, submitButton : Button) extends GridPane {
  qaLabIcon setOnMouseClicked new EventHandler[MouseEvent] {
    override def handle(event: MouseEvent) = {
      Controller.replaceSceneContent(stage, new AboutController(stage, qaLabIcon, answerSheetLabel, submitButton))
    }
  }
  this setAlignment Pos.CENTER
  this setHgap 10
  this setVgap 10
  this setPadding new Insets(25, 25, 25, 25)
  this setStyle "-fx-background-color:#ffffff"

  this add (qaLabIcon, 0, 0, 2, 1)
  this add (answerSheetLabel, 0, 1)
  this add (submitButton, 1, 1, 1, 1)
}

class AboutController(stage : Stage, qaLabIcon : Label, answerSheetLabel : Label, submitButton : Button) extends GridPane {
  qaLabIcon setOnMouseClicked new EventHandler[MouseEvent] {
    override def handle(event: MouseEvent) = {
      Controller.replaceSceneContent(stage, new HomeController(stage, qaLabIcon, answerSheetLabel, submitButton))
    }
  }
  this setAlignment Pos.CENTER
  this setHgap 10
  this setVgap 10
  this setPadding new Insets(25, 25, 25, 25)
  this setStyle "-fx-background-color:#ffffff"

  val version = new Hyperlink("NTCIR QALab Scorer 2016112001")
  version setOnMouseClicked new EventHandler[MouseEvent] {
    override def handle(event: MouseEvent): Unit = {
      if (Desktop.isDesktopSupported) {
        Desktop.getDesktop.browse(new URI("https://bitbucket.org/ntcirqalab/qalabsimplescorer"))
      }
    }
  }
  this add (qaLabIcon, 0, 0)
  this add (version, 0, 1)
  this add (new Label("Created by K. Sakamoto"), 0, 2)
  this add (new Label("(kotarosakamoto@nii.ac.jp)"), 0, 3)
}