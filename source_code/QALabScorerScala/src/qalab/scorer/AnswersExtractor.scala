package qalab.scorer

import scala.collection.mutable
import scala.xml.NodeSeq

/**
 * @author K.Sakamoto
 */
trait AnswersType {
  type AnswerAndScore = (NodeSeq, NodeSeq, Double)
  type Answers = Map[String, AnswerAndScore]
}

trait AnswersExtractor extends AnswersType {
  def extract(xml : NodeSeq) : Answers
}

object AnswersExtractorForCenterExam extends AnswersExtractor {
  override def extract(xml : NodeSeq) : Answers = {
    val answers = mutable.Map[String, AnswerAndScore]()
    var counter = 0
    xml \\ "data" foreach {data =>
      counter += 1
      val ansColumnID = (data \ "anscolumn_ID").text
      val answerColumnID = try {ansColumnID.substring(1).toInt} catch {case e : Exception => counter}
      val answer = data \ "answer"
      val score = try {(data \ "score").text.toDouble} catch {case e : Exception => 0D}
      answers(answerColumnID.toString) = (<td>{ansColumnID}</td>, answer, score)
    }
    answers.toMap
  }
}

object AnswersExtractorForSecondaryExam extends AnswersExtractor {
  override def extract(xml : NodeSeq) : Answers = {
    val answers = mutable.Map[String, AnswerAndScore]()
    var counter = 0
    xml \\ "answer_section" foreach {answer_section =>
      counter += 1
      val answerSectionID = (answer_section \ "@id").text
      val answerSectionLabel = (answer_section \ "@label").text
      val id = try {answerSectionID.split("-").last.toInt} catch {case e : Exception => counter}
      val answerColumnID = uniqueId(id.toString, answers, None)
      val answer = answer_section \ "answer_set"
      val score = 1D
      answers(answerColumnID) = (<td>{answerSectionID}</td> ++ <td>{answerSectionLabel}</td>, answer, score)
    }
    answers.toMap
  }

  private def uniqueId(id: String, map: mutable.Map[String, AnswerAndScore], charOpt: Option[Char]): String = {
    if (map contains id) {
      charOpt match {
        case Some(char) =>
          val newId = id concat char.toString
          if (map contains newId) {
            uniqueId(id, map, Option((char + 1).toChar))
          } else {
            newId
          }
        case None =>
          uniqueId(id, map, Option('a'))
      }
    } else {
      id
    }
  }
}