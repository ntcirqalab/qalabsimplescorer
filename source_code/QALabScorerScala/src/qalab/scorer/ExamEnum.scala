package qalab.scorer

/**
 * @author K.Sakamoto
 */
object ExamEnum extends Enumeration {
  val OTHER, CENTER_EXAM, SECONDARY_EXAM, QUESTION_ANALYSIS_RESULT = Value
}
