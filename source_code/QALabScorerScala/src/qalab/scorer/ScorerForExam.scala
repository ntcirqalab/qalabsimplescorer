package qalab.scorer

import java.text.Normalizer

import scala.xml.NodeSeq

/**
 * @author K.Sakamoto
 */
trait ScorerForExam {
  def getMark(systemAnswer : NodeSeq, correctAnswer : NodeSeq) : MarkEnum.Value
}

object ScorerForCenterExam extends ScorerForExam {
  override def getMark(systemAnswer : NodeSeq, correctAnswer : NodeSeq) =
    if (systemAnswer.text.trim == "") {
      MarkEnum.UNANSWERED
    } else if (systemAnswer.text.trim == correctAnswer.text.trim) {
      MarkEnum.CORRECT
    } else {
      MarkEnum.INCORRECT
    }
}

object ScorerForSecondaryExam extends ScorerForExam {
  override def getMark(systemAnswer : NodeSeq, correctAnswer : NodeSeq) =
    if (systemAnswer.text.trim == "") {
      MarkEnum.UNANSWERED
    } else if (isMatchTypeBroad(correctAnswer)) {
      MarkEnum.WITHHOLDING
    } else if (isCorrect(systemAnswer, correctAnswer)) {
      MarkEnum.CORRECT
    } else {
      MarkEnum.INCORRECT
    }

  private def isMatchTypeBroad(correctAnswer : NodeSeq) =
    (correctAnswer \ "answer" \ "@match_type").text == "broad"

  private def isCorrect(systemAnswer : NodeSeq, correctAnswer : NodeSeq) = {
    getCorrectAnswerExpressions(correctAnswer) contains
      getSystemAnswerExpression(systemAnswer)
  }

  private def getCorrectAnswerExpressions(correctAnswer : NodeSeq) = {
    (correctAnswer \ "answer" \ "expression_set" \ "expression") collect {
      case e => Normalizer.normalize(e.text.trim, Normalizer.Form.NFKC)
    }
  }

  private def getSystemAnswerExpression(systemAnswer : NodeSeq) =
    getCorrectAnswerExpressions(systemAnswer).headOption getOrElse ""
}