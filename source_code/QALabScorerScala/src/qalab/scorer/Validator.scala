package qalab.scorer

import java.io._
import java.nio.charset.StandardCharsets
import java.nio.file.{Files, Paths, StandardOpenOption}
import java.time.format.DateTimeFormatter
import java.time.{ZoneId, ZonedDateTime}
import java.util.Locale
import javax.xml.parsers.DocumentBuilderFactory
import javax.xml.transform.stream.StreamSource
import javax.xml.validation.SchemaFactory

import org.xml.sax._

import scala.io.Source

/**
 * @author K.Sakamoto
 */
object Validator {
  def validate(fileName: String) = {
    println("HERE2")
    println(fileName)
    val factory = DocumentBuilderFactory newInstance()
    factory setValidating true
    val builder = factory newDocumentBuilder()
    builder setEntityResolver new MyEntityResolver
    builder setErrorHandler   new MyErrorHandler
    val inputStream = new FileInputStream(fileName)
    try {
      builder parse inputStream
    } finally {
      try {
        inputStream close()
      } catch {
        case e: IOException =>
          e.printStackTrace()
      }
    }
  }

  def validate(fileName: String, exam: ExamEnum.Value) = {
    exam match {
      case ExamEnum.CENTER_EXAM =>
        validateNationalCenterTest(fileName)
      case ExamEnum.SECONDARY_EXAM =>
        validate2ndStageExam(fileName)
      case otherwise =>
    }
  }

  private def validateNationalCenterTest(fileName: String) = {
    val factory = DocumentBuilderFactory newInstance()
    factory setValidating true
    val builder = factory newDocumentBuilder()
    builder setEntityResolver new MyEntityResolver
    builder setErrorHandler   new MyErrorHandler
    val inputStream = new FileInputStream(fileName)
    try {
      builder parse inputStream
    } finally {
      try {
        inputStream close()
      } catch {
        case e: IOException =>
          e.printStackTrace()
      }
    }
  }

  private def validate2ndStageExam(fileName: String) = {
    val factory = SchemaFactory newInstance "http://www.w3.org/2001/XMLSchema"
    val schemaLocation = Paths.get("res", "schema", "second_stage_exam.xsd").toAbsolutePath.toFile
    val schema = factory newSchema schemaLocation
    val validator = schema newValidator()
    validator setErrorHandler new MyErrorHandler
    val source = new StreamSource(fileName)
    try {
      validator validate source
    } catch {
      case e: SAXException =>
        e.getMessage
    }
  }
}

object MyLogFile {
  private val logFile = createLogFile

  private def createLogFile = {
    new File("%1$s%2$s%3$s%2$s%4$s" format (
      System getProperty "user.home",
      File.separator,
      ".qalab",
      "log")
    )
  }

  def getLogFile = logFile

  def clearLogFile() {
    logFile.deleteOnExit()
  }

  def getLog =
    try {
      if (logFile.canRead) {
        val builder = new StringBuilder
        val source = Source fromFile logFile
        for (line <- source getLines()) {
          builder.append(line).
                  append(System lineSeparator())
        }
        Option(builder result()) match {
          case Some(result) =>
            result
          case None =>
            ""
        }
      } else {
        val logParentPath = logFile.toPath.getParent
        if (!logParentPath.toFile.canRead) {
          Files.createDirectory(logParentPath)
        }
        logFile.createNewFile()
        ""
      }
    } catch {
      case e : IOException =>
        e.printStackTrace()
        ""
    }
}

class MyErrorHandler extends ErrorHandler {
  private val logPath = MyLogFile.getLogFile.toPath

  private def getLogFormat(errorCode : String, lineNumber : Int, columnNumber : Int, message : String) =
    "%s :: [%s] line %d, column %d, %s" format (
      //new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss Z", Locale.US) format new Date(Calendar.
      //  getInstance(TimeZone getTimeZone "Asia/Tokyo", Locale.US).getTimeInMillis),
      DateTimeFormatter.ofPattern("EEE, d MMM yyyy HH:mm:ss Z", Locale.US).
        format(ZonedDateTime.now(ZoneId.of("Asia/Tokyo"))),
      errorCode, lineNumber, columnNumber, message)

  @throws(classOf[SAXException])
  override def warning(e : SAXParseException) {
    val writer = new PrintWriter(Files.newBufferedWriter(logPath, StandardCharsets.UTF_8))
    try {
      val message = getLogFormat("warning", e.getLineNumber, e.getColumnNumber, e.toString)
      writer.println(message)
      println(message)
    } catch {
      case e : IOException =>
        e.printStackTrace(writer)
    } finally {
      try {
        writer close()
      } catch {
        case e : IOException =>
          e.printStackTrace(writer)
      }
    }
  }

  @throws(classOf[SAXException])
  override def error(e : SAXParseException) {
    val writer = new PrintWriter(Files.newBufferedWriter(logPath,
      StandardCharsets.UTF_8,
      StandardOpenOption.CREATE
    ))
    try {
      val message = getLogFormat("error", e.getLineNumber, e.getColumnNumber, e.toString)
      writer.println(message)
      println(message)
    } catch {
      case e : IOException =>
        e.printStackTrace(writer)
    } finally {
      try {
        writer close()
      } catch {
        case e : IOException =>
          e.printStackTrace(writer)
      }
    }
  }

  @throws(classOf[SAXException])
  override def fatalError(e : SAXParseException) {
    val writer = new PrintWriter(Files.newBufferedWriter(logPath, StandardCharsets.UTF_8))
    try {
      val message = getLogFormat("fatal error", e.getLineNumber, e.getColumnNumber, e.toString)
      writer.println(message)
      println(message)
    } catch {
      case e : IOException => e.printStackTrace(writer)
    } finally {
      try {
        writer close()
      } catch {
        case e : IOException => e.printStackTrace(writer)
      }
    }
  }
}

class MyEntityResolver extends EntityResolver {
  @throws(classOf[SAXException])
  @throws(classOf[IOException])
  override def resolveEntity(publicId : String, systemId : String) : InputSource = {
    val source = new InputSource
    source setPublicId ""
    source setSystemId Paths.get("res", "schema", "national_center_test.dtd").
      toAbsolutePath.toString
    source
  }
}
