package qalab.scorer

/**
 * @author K.Sakamoto
 */
object MarkEnum extends Enumeration {
  val ERROR, UNANSWERED, WITHHOLDING, CORRECT, INCORRECT = Value

  def toString(mark : MarkEnum.Value) =
    mark match {
      case MarkEnum.UNANSWERED =>
        "unanswered"
      case MarkEnum.WITHHOLDING =>
        "withholding"
      case MarkEnum.CORRECT =>
        "correct"
      case MarkEnum.INCORRECT =>
        "incorrect"
      case _ =>
        "error"
    }
}
