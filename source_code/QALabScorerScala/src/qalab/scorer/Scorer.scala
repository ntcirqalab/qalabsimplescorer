package qalab.scorer

import java.io.File

import scala.collection.mutable
import scala.xml.{XML, NodeSeq}

/**
 * @author K.Sakamoto
 */
object Scorer extends Scorer

class Scorer extends AnswersType {
  private type Marks = Map[Integer, MarkEnum.Value]
  private type Correction = (MarkEnum.Value, AnswerAndScore, AnswerAndScore)
  private type Corrections = Map[String, Correction]
  private var exam = ExamEnum.OTHER

  def getCorrections(systemAnswersFile : File, exam : ExamEnum.Value, examData : ExamDataEnum.Value) : Corrections = {
    setExam(exam)
    val systemAnswersXML = XML loadFile systemAnswersFile
    val correctAnswersXML = XML loadFile
      (ExamDataEnum getCorrectAnswersFile systemAnswersFile)
    getCorrections(
      extract(systemAnswersXML),
      extract(correctAnswersXML))
  }

  def getValidations(systemAnswersFile : File, exam : ExamEnum.Value) : Answers = {
    setExam(exam)
    val systemAnswersXML = XML loadFile systemAnswersFile
    extract(systemAnswersXML)
  }

  private def getCorrections(systemAnswers : Answers, correctAnswers : Answers) = {
    val ansColumnIds = correctAnswers.keySet
    val corrections = mutable.Map[String, Correction]()
    for (ansColumnId <- ansColumnIds) {
      val correctAnswer = correctAnswers(ansColumnId)
      if (systemAnswers.contains(ansColumnId)) {
        val systemAnswer = systemAnswers(ansColumnId)
        corrections(ansColumnId) = (
          getMark(systemAnswer._2, correctAnswer._2),
          systemAnswer, correctAnswer)
      } else {
        corrections(ansColumnId) = (
          MarkEnum.UNANSWERED,
          (NodeSeq.Empty, NodeSeq.Empty, 0D),
          correctAnswer)
      }
    }
    corrections.toMap
  }

  private def extract(answers : NodeSeq) : Answers =
    exam match {
      case ExamEnum.CENTER_EXAM =>
        AnswersExtractorForCenterExam extract answers
      case ExamEnum.SECONDARY_EXAM =>
        AnswersExtractorForSecondaryExam extract answers
      case _ => Map.empty[String, AnswerAndScore]
    }

  private def getMark(systemAnswer : NodeSeq, correctAnswer : NodeSeq) =
    exam match {
      case ExamEnum.CENTER_EXAM =>
        ScorerForCenterExam getMark (systemAnswer, correctAnswer)
      case ExamEnum.SECONDARY_EXAM =>
        ScorerForSecondaryExam getMark (systemAnswer, correctAnswer)
      case _ => MarkEnum.ERROR
    }

  private def setExam(exam : ExamEnum.Value) {
    this.exam = exam
  }
}