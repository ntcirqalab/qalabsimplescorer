package qalab;

import qalab.scorer.gui.ScorerStage;

/**
 * @author K.Sakamoto
 */
public class Main extends ScorerStage {
    public static void main(String[] args) {
        launch(args);
    }
}
