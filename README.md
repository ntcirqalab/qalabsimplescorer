﻿#QALab Simple Scorer

##1. 実行環境
EclipseによるJava開発環境が準備されている必要があります。

---
##2. Scorerの取得

次のURLのDownload repositoryをクリックして、ファイルをダウンロードしてください。

[https://bitbucket.org/ntcirqalab/qalabsimplescorer/downloads](https://bitbucket.org/ntcirqalab/qalabsimplescorer/downloads)

Gitを用いて取得する場合は、
~~~~
$ cd リポジトリを作成するディレクトリ
$ git clone https://bitbucket.org/ntcirqalab/factoidqa-centerexam.git
~~~~
または、
~~~~
$ cd リポジトリを作成するディレクトリ
$ git clone git@bitbucket.org:ntcirqalab/factoidqa-centerexam.git
~~~~

---
##3. Contents of this repository
* システムの回答をDTDによるXMLファイルの妥当性検証を行い、Scorerが実際に受け取る回答を表示するためのFormat Checker
* システムの回答を採点し、採点結果を表示するためのScorer
* EclipseでFormat CheckerとScorerを起動するためのworkspace/
* Format CheckerとScorerの動作確認用のデータsample/
* Format CheckerとScorerのソースコードsource_code/

###Folder Structures

The overall folder structure:
~~~~
ntcirqalab-qalabsimplescorer/ 
├── README.md
├── sample/			/* スコアラの動作確認用のデータです。 */ 
├── source_code/	/* スコアラのソースコードです。ソースコードを読みたい人用に置きました。 */ 
├── workspace/		/* Eclipseでスコアラを起動するためのworkspaceです。 */ 
└── .gitignore
~~~~

---
##4. Usage
（sampleを使って動作確認する場合は、(1)と(2)を飛ばして読んでください。）

1. スコアラに投げる最終結果のXMLファイルの名前を、提出用のフォーマット（[http://research.nii.ac.jp/qalab/#format](http://research.nii.ac.jp/qalab/#format)）に従って命名してください（以下、提出用XMLファイルとする）。
2. 提出用XMLファイルと同じ場所にansディレクトリを作成してください。

###Format Checker
3. 正解のXMLファイルがansディレクトリ下にあれば、ansディレクトリ下から取り除いてください。
4. eclipseのワークスペースとしてworkspaceを指定してeclipseを起動します。
5. workspaceの中に含まれているQALabSimpleScorerプロジェクトのMain.javaをメインクラスに指定してRunすると、GUIが起動します。
6. 「Submit」ボタンを押して、提出用のXMLファイルを選択してください。（sampleを使って動作確認する場合は、「Center-2009--Main-SekaishiB_TeamID_EN_FA_01.xml」というファイルを選択してください。）
7. フォーマットが正しければ、提出用のXMLファイルと同じ場所にHTMLファイルが出力され、Webブラウザに表示されます。

正常にHTMLを出力できない場合は、eclipseのConsole上にエラーが出力されます。

DTDのチェックに失敗している場合は、ホームディレクトリに作成される.qalabディレクトリの直下のlogファイルにもエラー文が書き出されま
す。この場合は、XMLファイルを修正して、logファイルを削除したのち、スコアラーをRunしてください。

###Scorer
3. 正解のXMLファイルをansディレクトリ下に入れてください。
4. eclipseのワークスペースとしてworkspaceを指定してeclipseを起動します。
5. workspaceの中に含まれているQALabSimpleScorerプロジェクトのMain.javaをメインクラスに指定してRunすると、GUIが起動します。
6. 「Submit」ボタンを押して、提出用のXMLファイルを選択してください。（sampleを使って動作確認する場合は、「Center-2009--Main-SekaishiB_TeamID_EN_FA_01.xml」というファイルを選択してください。）
7. 提出用のXMLファイルと同じ場所にHTMLファイルが出力され、Webブラウザに表示されます。

sampleに含まれている「Center-2009--Main-SekaishiB_TeamID_EN_FA_01.result.html」というファイルは、Scorerの出力です。

Video:

[![Video](//img.youtube.com/vi/Qx8imC6j2fU/0.jpg)](//www.youtube.com/embed/videoseries?list=PLgQMIdBNegcLfWWZij-Wc-VnvRcGLC2LX)

---
##5. 表示されるHTMLファイルの読み方

###Format Checker

####最初のテーブル
参加者のシステムが出力した回答の数が表示されます。

|||
|-|-|
|THE NUMBER OF ANSWERS|参加者のシステムが出力した回答の数です。|


####２つ目のテーブル

|カラム|説明|
|-|-|
|anscolumn_ID (センター試験用)|anscolumn_IDです。|
|answer_section's id attribute (２次試験用)|answer_sectionタグのid属性です。|
|answer_section's label attribute (２次試験用)|answer_sectionタグのlabel属性です。|
|YOUR ANSWER|参加者のシステムが出力した回答です。|

###Scorer

####最初のテーブル

|||
|-|-|
|SCORE|「獲得した点数 / 全問正解した場合の点数」です。|
|CORRECT|正解の数です。|
|INCORRECT|不正解の数です。|
|WITHHOLDING|2次試験用に作成したものなので、センター試験では必ず0です。2次試験では、論述問題のような人の手を介さないと採点できない問題があるので、そういった保留の場合は、ここのカウントが増えます。|
|UNANSWERED|解答しなかった問題の数です。|
|ERROR|エラーで読み取れていない問題の数です。|

####２つ目のテーブル

|カラム|説明|
|-|-|
|anscolumn_ID (センター試験用)|anscolumn_IDです。|
|answer_section's id attribute (２次試験用)|answer_sectionタグのid属性です。|
|answer_section's label attribute (２次試験用)|answer_sectionタグのlabel属性です。|
|RESULT|correct, incorrect, withholding, unanswered, errorのいずれかが入ります。|
|YOUR ANSWER|参加者のシステムが出力した回答です。|
|CORRECT ANSWER|正解情報です。|
|SCORE|配点です。|

---
##6. Version
2017112001

---
November 20, 2016

Kotaro Sakamoto, kotarosakamoto [at] nii.ac.jp